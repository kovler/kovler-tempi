package kovler;

import java.util.Scanner;
import java.util.Random;

public class Ass4 {

	public static void main (String[] args) {
		int givenEx = 0;//user's number of exercises.
		int rightEx = 0;//user's number of right answers.
		int lvl = getLvl();//asking the user for his level from 1-3.
		char op = getOp(lvl);//asking the user which operation does he/she want to practice.
		boolean again;
		do {
		boolean correct = play(lvl, op);//asking the user math questions.
		if (correct == true)
			rightEx++;//if the user is right he got one point for the right answers.
		if (rightEx%5==0&&lvl<=3)
			lvl++;//for every five right answers the user goes up 1 level.
		givenEx++;
		again = wantsAgain();// asking the user if he want to play again.
		}
		while (again);
		showResults(givenEx, rightEx);//showing the user his result.
	}
	
	public static int getLvl() {// get a number from a user
		Scanner in = new Scanner(System.in);
		System.out.println("please choose you math level, \n 1.begginer \n 2.advanced \n 3.expert");
		String num = in.next();
		while (!(num.equals("3"))&&!(num.equals("1"))&&!(num.equals("2"))){
			System.out.println("invalid input, please choose you math level, \n 1.begginer \n 2.advanced \n 3.expert");
			num = in.next();
		}
		int k = Integer.valueOf(num);
		return (k);
	}
	
	public static char getOp(int lvl) {//get the operation the user want to practice
		Scanner in = new Scanner(System.in);
		String s = "";
		if (lvl == 3){
			System.out.println("which operation do you want to practice? \n 1.'+' \n 2.'-' \n 3.'*' \n 4.'/'");
			s = in.nextLine();
			while (!(s.equals("1"))&&!(s.equals("2"))&&!(s.equals("3"))&&!(s.equals("4"))){
				System.out.println("invalid input, which operation do you want to practice? \n 1.'+' \n 2.'-' \n 3.'*' \n 4.'/'");
				s = in.nextLine();
			}
		}
		else {
			System.out.println("which operation do you want to practice? \n 1.'+' \n 2.'-'");
			s = in.nextLine();
			while (!(s.equals("1"))&&!(s.equals("2"))){
				System.out.println("invalid input, which operation do you want to practice? \n 1.'+' \n 2.'-'");
				s = in.nextLine();
			}
		}
	char c = s.charAt(0);
	return (c);
	}
	
	public static boolean play (int lvl, char op){//give user math question according to their level
		Random r = new Random();
		Scanner in = new Scanner(System.in);
		
		// lvl 1 plus operation
		if (lvl == 1 && op == 49){
			System.out.println("your question is:");
			int a = r.nextInt(11);
			int b = r.nextInt(11);
			System.out.println(a+ "+" +b+"=");
			int c = in.nextInt();
			if (a+b == c){
				System.out.println("correct!");
				return true;
			}//if
				System.out.println("false!");
				return false ;
		}//if
		
		//lvl 2 plus operation
		if (lvl == 2 && op == 49){
			int a = r.nextInt(26);
			int b = r.nextInt(26);
			System.out.println(a+ "+" +b+"=");
			int c = in.nextInt();
			if (a+b == c){
				System.out.println("correct!");
				 return true;
			}//if
				System.out.println("false!");
				return false;
		}//if
		
		//lvl 3 plus operation
		if (lvl == 3 && op == 49){
			int a = r.nextInt(31)+20;
			int b = r.nextInt(31)+20;
			System.out.println(a+ "+" +b+"=");
			int c = in.nextInt();
			if (a+b == c){
				System.out.println("correct!");
				 return true;
			}//if
				System.out.println("false!");
				return false ;
		}//if
		
		//lvl 1 minus operation
		if (lvl == 1 && op == 50){
			int a = r.nextInt(21);
			int b = r.nextInt(21);
			System.out.println(a+ "-" +b+"=");
			int c = in.nextInt();
			if (a-b == c){
				System.out.println("correct!");
				 return true;
			}//if
				System.out.println("false!");
				return false ;
		}//if
		
		//lvl 2 minus operation
		if (lvl == 2 && op == 50){
			int a = r.nextInt(51);
			int b = r.nextInt(51);
			System.out.println(a+ "-" +b+"=");
			int c = in.nextInt();
			if (a-b == c){
				System.out.println("correct!");
				 return true;
			}//if
				System.out.println("false!");
				return false ;
		}//if
		
		//lvl 3 minus operation
		if (lvl == 3 && op == 50){
			int a = r.nextInt(81)+20;
			int b = r.nextInt(81)+20;
			System.out.println(a+ "-" +b+"=");
			int c = in.nextInt();
			if (a-b == c){
				System.out.println("correct!");
				 return true;
			}//if
				System.out.println("false!");
				return false ;
		}//if
		
		//lvl 3 multiply operation
		if (lvl == 3 && op == 51){
			int a = r.nextInt(81)+20;
			int b = r.nextInt(81)+20;
			System.out.println(a+ "*" +b+"=");
			int c = in.nextInt();
			if (a*b == c){
				System.out.println("correct!");
				 return true;
			}//if
				System.out.println("false!");
				return false ;
		}//if
		
		//lvl 3 division operation
				if (lvl == 3 && op == 52){
					int a = r.nextInt(81)+20;
					int b = r.nextInt(81)+20;
					System.out.println(a+ "/" +b+"=");
					int c = in.nextInt();
					if (a/b == c){
						System.out.println("correct!");
						 return true;
					}//if
						System.out.println("false!");
						return false ;
				}//if
		
		return true;
		
	}//play

	public static boolean wantsAgain() {//asking the user if he wants to play again
		Scanner in = new Scanner(System.in);
		System.out.println("do yo want to play again? \n 'y'- yes \n 'n'-no");
		String s = in.nextLine();
		while (!(s.equals("y"))&&!(s.equals("n"))&&!(s.equals("Y"))&&!(s.equals("N"))){
			System.out.println("invalid input, do yo want to play again? \n 'y'- yes \n 'n'-no");
			s = in.nextLine();
		}//while
		if (s.equals("y")||s.equals("Y"))
			return true;
		else 
			return false;
	}//wantsAgain

	public static void showResults(int givenEx, int rightEx){//showing user's result
		System.out.println("you have " +rightEx+ " out of " +givenEx);
		double a = rightEx;
		double b = givenEx;
		double r = a/b*100;
		System.out.println("your score is " +r);
	}
}