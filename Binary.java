package kovler;

import java.util.Scanner;

public class Binary {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int q = 0;
		while (q == 0){
		System.out.print("enter a positive number:");
		int n = in.nextInt();
		while(n<0){
			System.out.print("please enter a positive number");
			n = in.nextInt();
		}
	    double result = 0;
	    int z = 0;
	    
	    while (n>0){
	    	if (n%2>0){
	    	result = result + Math.pow(10, z);
	    	}
	    	n=n/2;
	    	z=z+1;
		}
	    System.out.print(result);
		System.out.println();
		}
	}

}
