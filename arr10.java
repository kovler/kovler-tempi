package kovler;

public class arr10 {

	public static void main(String[] args) {
		String[] arr1 = {"hello"};
		String[] arr2 = {"itc","bootcamp"};
		String[] arr3 = addArrays(arr1, arr2);
		for (int i = 0; i<arr3.length; i++)
			System.out.println(arr3[i]);
	}
	public static String[] addArrays(String[] arr1, String[] arr2){
		String[] arr3 = new String[arr1.length+arr2.length];
		for (int i = 0; i<arr1.length; i++){
			arr3[i] = arr1[i]; 
		}
		for (int j = arr1.length; j< arr2.length+arr1.length; j++){
			arr3[j] = arr2[j-arr1.length];
		}
		return (arr3);
	}
}
