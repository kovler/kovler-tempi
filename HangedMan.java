package kovler;

import java.util.Random;
import java.util.Scanner;


public class HangedMan {
	static final String[] words = {"wonderland", "theory", "game"};
	public static void main(String[] args){
		String secretWord = getRandomWord().toLowerCase();
		Scanner in = new Scanner(System.in);
		Random rn = new Random();
		
		//welcome
		System.out.println("   |/| ");
		System.out.println("   | | ");
		System.out.println("   |/| ");
		System.out.println("   | | ");
		System.out.println("   |/| ");
		System.out.println("  (___) ");
		System.out.println("  (___) ");
		System.out.println("  (___) ");
		System.out.println("  (___) ");
		System.out.println("  // \\ ");
		System.out.println(" //   \\ ");
		System.out.println("||     || ");
		System.out.println("||     || ");
		System.out.println("||     || ");
		System.out.println("  \\___//  ");
		System.out.println("   ---   ");
		
		//intro
		System.out.println("welcome to the hanged man game!");
		in.nextLine();
		System.out.println("your job is to guess what is the word that's hiding in 10 chances.");
		in.nextLine();
		System.out.println("if you chose a right letter your chances balance will stay");
		in.nextLine();
		System.out.println("please press enter to start:");
		in.nextLine();
		
		//variables
		int r = rn.nextInt(3);
		String s = words[r];
		int les = s.length();
		String x = "";
		
		//the word
		for (int i = 0; i<les ; i++){
			x = x +"*";
		}
		System.out.print(x);
		
		//formula
		int count1 = 10;
		while (count1>0){
			System.out.println();
			System.out.println("you now have " +count1+ " chances:");
			System.out.println("please choose a letetr:");
			String q = in.nextLine();
			char ch = q.charAt(0);
			
			//if not a letter
			while (!(ch <= 122) || !(ch >= 97)){
				System.out.println("invalid input, please enter one letter!:");
				q = in.nextLine();
				ch = q.charAt(0);
			}
			
			//if more than one letter
			while (q.length()!= 1){
				System.out.println("please enter just one letter:");
				q = in.nextLine();
				ch = q.charAt(0);
			}
			
			//if true
			if (s.indexOf(ch) != -1){
				for (int i = 0 ; i<les ; i++){
					char c = s.charAt(i);
					if  (c == ch){
						x = x.substring(0, i) + ch + x.substring(i+1);				
					}
				}
				System.out.println("yap!");
				System.out.println(x);
			}
			
			
			//if false
			if (s.indexOf(ch) == -1) {
				System.out.println("nope, the letter " +ch+ " ain't belong to this word");
				count1--;
			}
			
			//if finished
			if (x.equals(s)){
				System.out.println("BRAVO!!!");
				System.exit(0);
			}
		}
		System.out.println("failed. goodbye");
		

		
	}

	public static String getRandomWord(){
		Random r = new Random();
		int wordIndex = r.nextInt(words.length);
		return words[wordIndex];
	}

}