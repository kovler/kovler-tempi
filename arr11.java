package kovler;

public class arr11 {

	public static void main(String[] args) {
		String[] arr1 = {"hello","hi","hey"};
		String[] arr2 = {"itc","bootcamp"};
		String[] arr3 = joinArrays(arr1, arr2);
		for (int i = 0; i<arr3.length; i++){
			System.out.println(arr3[i]);
		}
	}
	public static String[] joinArrays(String[] arr1, String[] arr2){
		String[] arr3 = new String[arr1.length*arr2.length];
		int count = 0;
		for (int i = 0; i<arr1.length; i++){
			for(int j = 0; j<arr2.length; j++){
				arr3[count] = arr1[i]+" " +arr2[j];
				count++;
			}
		}
		return (arr3);
	}
}
