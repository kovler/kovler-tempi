package kovler;

import java.util.Scanner;

public class Grade {
	
	public static void main(String[] args) {
		int e = 0;
		while (e == 0){
		Scanner in = new Scanner(System.in);
		System.out.print("how many grades do you have?:");
		int n = in.nextInt();
		while (n>10 || n<=0){
			System.out.print("bad number,\n please enter a number between 1-10:");
			n = in.nextInt();
		}
		double total = 0;
		for (int i = 1 ; i<n+1 ; i++){
			System.out.print("please enter grade number " +i+ ":");
			total += in.nextInt();
		}
		System.out.print(total / n);
		System.out.println();
}
}
}