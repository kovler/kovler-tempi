package kovler;

public class arr9 {

	public static void main(String[] args) {
		int[] arr1 = {1,2,3};
		int[] arr2 = {1,4,6};
		int[] arr3 = addArrays(arr1, arr2);
		for (int i = 0; i< arr3.length; i++){
			System.out.print(arr3[i]+",");
		}
	}
	
	public static int[] addArrays(int[] arr1, int[] arr2){
		int[] arr3 = new int[arr1.length];
		for (int i = 0; i<arr1.length; i++){
			arr3[i]= arr1[i]+arr2[i];
		}
		return (arr3);
	}


}
