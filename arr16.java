package kovler;

import java.util.Scanner;

public class arr16 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.println("please enter a number");
		int n1 = in.nextInt();
		System.out.println("please choose operation:\n press'1' for '+' \n press '2' for '-'");
		int op = in.nextInt();
		System.out.println("enter a number");
		int n2 = in.nextInt();
		
		int[] numNew = addition(n1 ,op, n2);
		for (int i =0 ; i<numNew.length; i++)
			System.out.println(numNew[i]);
	}
	
	public static int[] addition(int n1, int op, int n2){
		//finding the result
		int nNew = 0;
		if (op == 1)
			nNew = n1+n2;
		if (op == 2)
			nNew = n1-n2;
		
		//if the result if a negative number
		int minus = (nNew*-1); //the positive number of a negative outcome.
		String s_minus = ""+minus;// the string of the above
		int[] numsNegative = new int[s_minus.length()];//the return in case of negative outcome
		if (nNew<0){
			for (int i = s_minus.length()-1; i>=0; i--){
				if (i == 0)
					minus = minus*-1;
				numsNegative[i] = minus%10;
				minus = minus/10;
			}
		}
		
		//if the result is a positive number
		int plus = nNew;
		String s_positive = ""+plus;
		int[] numsPositive = new int[s_positive.length()];
		for (int j = s_positive.length()-1; j>=0; j--){
			numsPositive[j] = plus%10;
			plus = nNew/10;
		}
		if (nNew>=0)
			return (numsPositive);
		else 
			return (numsNegative);
	}
}
